data "http" "cloudflare_ips" {
  url             = "https://www.cloudflare.com/ips-v4"
  request_headers = {
    Accept = "application/json"
  }
}

data "http" "maxmind" {
  for_each = toset([for location in split("\n", data.http.cloudflare_ips.body) : split("/", location)[0]])
  url      = "http://ip-api.com/json/${each.value}?fields=continentCode,countryCode"
}

output "eu_ips" {
  value = [
  for key, value in [
  for key, value in data.http.maxmind : { ip : key, code : jsondecode(value.body).continentCode }
  ] : value.ip if value.code == "EU"
  ]
}
